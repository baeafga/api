Rails.application.routes.draw do
  resources :tests
  post '/get_token', to: 'token#get_token'
  post '/change_token', to: 'token#change_token'
end

class TokenController < ApplicationController

  # POST /change_token
  def change_token
    if token_params[:auth_secret] == ENV['BAEA_AUTH_SECRET']
      if valid_token(token_params[:new_auth_token])
        ENV['BAEA_AUTH_TOKEN'] = token_params[:new_auth_token].upcase
        token = ENV['BAEA_AUTH_TOKEN']
        render json: token, status: :ok
      else
        render json: {'error': 'Unprocessable Entity'}, status: :unprocessable_entity
      end
    else
      render json: {'error': 'Unauthorized'}, status: :unauthorized
    end
  end

  # POST /get_token
  def get_token
    if token_params[:auth_secret] == ENV['BAEA_AUTH_SECRET']
      token = ENV['BAEA_AUTH_TOKEN']
      render json: token
    else
      render json: {'error': 'Unauthorized'}, status: :unauthorized
    end
  end

  private
    def token_params
      params.require(:token).permit(:auth_secret, :new_auth_token)
    end

    def valid_token(token)
      token && token.length >= 8 && token.length <= 55
    end

end

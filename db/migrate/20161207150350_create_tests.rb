class CreateTests < ActiveRecord::Migration[5.0]
  def change
    create_table :tests do |t|
      t.decimal :angle
      t.decimal :velocity
      t.decimal :intended_velocity
      t.decimal :drag
      t.decimal :lift
      t.boolean :reset, default: false

      t.timestamps
    end
  end
end
